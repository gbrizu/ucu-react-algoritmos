import './App.css';
import { useState } from 'react';

function App() {

  const [products, setProducts] = useState([
    { id: 1, name: 'Producto 1', quantity: 0 },
    { id: 2, name: 'Producto 2', quantity: 0 },
    { id: 3, name: 'Producto 3', quantity: 0 },
    { id: 4, name: 'Producto 4', quantity: 0 },
    { id: 5, name: 'Producto 5', quantity: 0 },
    {id: 6, name:'Teclados', quantity: 0},
    {id: 7, name:'Mouses', quantity: 0},
    {id: 8, name:'Impresoras', quantity: 0},
  ]);

  const handleIncrement = (id) => {
    setProducts(prevProducts => {
      return prevProducts.map(product => {
        if (product.id === id) {
          return { ...product, name: product.name + product.id }
        }
        return product;
      })
    })
  }

  const handleDecrement = (id) => {
    setProducts(prevProducts => {
      return prevProducts.map(product => {
        if (product.id === id) {
          return { ...product, name: product.name.slice(0, -1) } // Eliminar el ultimo caracter
        }
        return product;
      })
    })
  }

return (
  <div>
    <h1>Shopping Cart</h1>
    <table>
      <thead>
        <tr>
          <th>ID</th>
          <th>Name</th>
          <th>Quantity</th>
        </tr>
      </thead>  
        <tbody>
          {products.map(product => {
            return (
          <tr>
            <td>{products.id}</td>
            <td>{products.name}</td>
            <td>{products.quantity}</td>
            <td><button onClick={()=>handleIncrement(products.id)}> + </button></td>
            <td><button onClick={()=>handleDecrement(products.id)}> - </button></td>
          </tr>
            )
          })}
        </tbody>
    </table>
    <table>
      <thead>
        <tr>
          <th>ID</th>
          <th>Name</th>
          <th>Quantity</th>
        </tr>
        </thead>
        <tbody>
          {products.map(product => {
            return (
              <tr>
                <td>{product.id}</td>
                <td>{product.name}</td>
                <td>{product.quantity}</td>
                <td><button onClick={()=>handleIncrement(product.id)}> + </button></td>
                <td><button onClick={()=>handleDecrement(product.id)}> - </button></td>
              </tr>
            )
          })}
        </tbody>
    </table>
  </div>
)

}



export default App;
