const express = require('express')
const app = express()
const port = 4001

app.use(express.json())

//Endpoint to get list of products
app.get('/products', (req, res) =>{
    //simulation of products
    const products = [
        {id: 1, name:'product 1', quentity: 0},
        {id: 2, name:'product 2', quentity: 0},
        {id: 3, name:'product 3', quentity: 0},
        {id: 4, name:'product 4', quentity: 0},
        {id: 5, name:'product 5', quentity: 0}
      ]

    res.json(products)  
})


//init server
app.listen(port, ()=>{
    console.log('Servidor express listen in the port' ,port)
})